#include <jni.h>
#include <string>
#include "cert.h"

// todo change to true on release build or forever if the same keystore is used
bool checkCert = false;
// todo try to obfuscate it somehow
/** you can get it from logcat if [checkCert] is false */
const char *RSA = "b239ee9564faf6dbfd1d3d71cff4aee912a83493452f7e092d63945eef47f12e1f1a4984060799c245e34959ab300d5a9f94b23de60dbbcf8e5ce6680df1cefde8a924dea3b6ea6c597ce96488ff46e13a0e045fcf91a10e4e7836546c97874fad8fbcd804741e5f4d5ee2046fa07397b0ae66360fe782404b50cb16d0bdee4e03f30d84a0842e09410209072b5b0efe84a1b2aafd983eb2809ce7e6d3dbcba19d802bc582cf004db0e825cbe4fcd4a9adc634c04a546bcac87390749ce175b9ae8c04edbe9f16e3634287d1e87008c02ca1a874149f310a121900af724dc28131054ef40a9769b5887ec14459a69e2ddca69c5dd3fc90a5bb71faf7dfc16b8b";

jboolean isCertValid = false;

extern "C" JNIEXPORT void JNICALL
Java_defpackage_certcheck_MainActivity_init(JNIEnv *env, jobject, jobject context) {
    isCertValid = checkCertificate(env, context);
}

extern "C" JNIEXPORT jstring JNICALL
Java_defpackage_certcheck_MainActivity_stringFromJNI(JNIEnv *env, jobject) {
    std::string hello;
    if (isCertValid) {
        hello = "Certificate is valid";
    } else {
        hello = "Certificate is invalid";
    }
    return env->NewStringUTF(hello.c_str());
}