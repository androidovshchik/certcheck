#include <string>
#include "cert.h"
#include <android/log.h>

#define TAG "CERT"

#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, TAG, __VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, TAG, __VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, TAG, __VA_ARGS__)

jboolean checkCertificate(JNIEnv *env, jobject ctx) {
    jclass cls = env->GetObjectClass(ctx);
    jmethodID mid = env->GetMethodID(cls, "getPackageManager",
                                     "()Landroid/content/pm/PackageManager;");
    jmethodID pnid = env->GetMethodID(cls, "getPackageName", "()Ljava/lang/String;");
    if (mid == 0 || pnid == 0) {
        return -1;
    }
    jobject pacManO = env->CallObjectMethod(ctx, mid);
    jclass pacMan = env->GetObjectClass(pacManO);
    jstring packName = (jstring) env->CallObjectMethod(ctx, pnid);
    /*flags = PackageManager.GET_SIGNATURES*/
    int flags = 0x40;
    mid = env->GetMethodID(pacMan, "getPackageInfo",
                           "(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");
    if (mid == 0) {
        return -1;
    }
    jobject packInfO = (jobject) env->CallObjectMethod(pacManO, mid, packName, flags);
    jclass packInf = env->GetObjectClass(packInfO);
    jfieldID fid;
    fid = env->GetFieldID(packInf, "signatures", "[Landroid/content/pm/Signature;");
    jobjectArray signatures = (jobjectArray) env->GetObjectField(packInfO, fid);
    jobject signature0 = env->GetObjectArrayElement(signatures, 0);
    mid = env->GetMethodID(env->GetObjectClass(signature0), "toByteArray", "()[B");
    jbyteArray cert = (jbyteArray) env->CallObjectMethod(signature0, mid);
    if (cert == 0) {
        return -1;
    }
    jclass bais = env->FindClass("java/io/ByteArrayInputStream");
    if (bais == 0) {
        return -1;
    }
    mid = env->GetMethodID(bais, "<init>", "([B)V");
    if (mid == 0) {
        return -1;
    }
    jobject input = env->NewObject(bais, mid, cert);
    jclass CF = env->FindClass("java/security/cert/CertificateFactory");
    mid = env->GetStaticMethodID(CF, "getInstance",
                                 "(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;");
    jstring X509 = env->NewStringUTF("X509");
    jobject cf = env->CallStaticObjectMethod(CF, mid, X509);
    if (cf == 0) {
        return -1;
    }
    //"java/security/cert/X509Certificate"
    mid = env->GetMethodID(CF, "generateCertificate",
                           "(Ljava/io/InputStream;)Ljava/security/cert/Certificate;");
    if (mid == 0) {
        return -1;
    }
    jobject c = env->CallObjectMethod(cf, mid, input);
    if (c == 0) {
        return -1;
    }
    jclass X509Cert = env->FindClass("java/security/cert/X509Certificate");
    mid = env->GetMethodID(X509Cert, "getPublicKey", "()Ljava/security/PublicKey;");
    jobject pk = env->CallObjectMethod(c, mid);
    if (pk == 0) {
        return -1;
    }
    mid = env->GetMethodID(env->GetObjectClass(pk), "toString", "()Ljava/lang/String;");
    if (mid == 0) {
        return -1;
    }
    jstring all = (jstring) env->CallObjectMethod(pk, mid);
    char *allChar = (char *) env->GetStringUTFChars(all, nullptr);
    char *out = nullptr;
    if (allChar != nullptr) {
        char *startString = strstr(allChar, "modulus:");
        char *end = strstr(allChar, "public exponent");
        bool isJB = false;
        if (startString == nullptr) {
            //4.1.x
            startString = strstr(allChar, "modulus=");
            end = strstr(allChar, ",publicExponent");
            isJB = true;
        }
        if (startString != nullptr && end != nullptr) {
            int len;
            if (isJB) {
                startString += strlen("modulus=");
                len = end - startString;
            } else {
                startString += strlen("modulus:");
                len = end - startString - 5; /* -5 for new lines*/
            }
            out = new char[len + 2];
            strncpy(out, startString, len);
            out[len] = '\0';
        }
    }
    env->ReleaseStringUTFChars(all, allChar);
    if (!checkCert) {
        LOGI("RSA key is %s", out);
    }
    char *isFound = strstr(out, RSA);
    // при отладке сертификат не проверяем
    return !checkCert || isFound != nullptr;
}