#ifndef CERTCHECK_CERT_H
#define CERTCHECK_CERT_H

#include <jni.h>

extern bool checkCert;
extern const char *RSA;

jboolean checkCertificate(JNIEnv *env, jobject ctx);

#endif //CERTCHECK_CERT_H